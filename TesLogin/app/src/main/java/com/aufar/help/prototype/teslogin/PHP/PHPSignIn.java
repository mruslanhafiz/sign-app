package com.aufar.help.prototype.teslogin.PHP;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.aufar.help.prototype.teslogin.SignInActivity;
import com.aufar.help.prototype.teslogin.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Objects;

/**
 * Created by aufar on 9/26/2014.
 */
public class PHPSignIn extends AsyncTask<String, Void, String> {

    private Intent HomeActivity;
    private Context c;
    private TextView hasil;
    private String link = "http://aidindonesia.esy.es/login.php";

    public PHPSignIn(Intent i, Context c, TextView hasil) {
        this.HomeActivity = i;
        this.c = c;
        this.hasil = hasil;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String username = params[0].toString();
            String password = params[1].toString();
            String data  = URLEncoder.encode("username", "UTF-8")
                    + "=" + URLEncoder.encode(username, "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8")
                    + "=" + URLEncoder.encode(password, "UTF-8");
            URL url = new URL(link);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter
                    (conn.getOutputStream());
            wr.write( data );
            wr.flush();
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = new String();
            line = "Login berhasil";
            String[] res = new String[5];
            // Read Server Response
            /*while((line = reader.readLine()) != null)
            {
                sb.append(line);
                break;
            }*/
            for (int i=0; i<5; i++){
                String temp;
                if ((temp = reader.readLine()) != null){
                    res[i] = temp;
                }
            }
            User.Username = res[0];
            User.Name = res[1];
            User.Gender = res[2];
            User.Birthdate = res[3];
            User.ProfilePicture = res[4];
            return line;
        }catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }
    }
    @Override
    protected void onPostExecute(String result){
        if(result!="")
            c.startActivity(this.HomeActivity);
        else {
            hasil.setVisibility(View.VISIBLE);
            this.hasil.setText("Wrong username or password :(");
        }
    }
}
