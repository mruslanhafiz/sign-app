package com.aufar.help.prototype.teslogin.PHP;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by aufar on 9/26/2014.
 */
public class PHPSignUp extends AsyncTask<String, Void, String> {

    private TextView hasil;
    private Intent HomeActivity;
    private Context c;
    private String link = "http://aidindonesia.esy.es/register.php";

    public PHPSignUp(Context c, Intent HomeActivity, TextView hasil) {
        this.c = c;
        this.HomeActivity = HomeActivity;
        this.hasil = hasil;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            String username = params[0].toString();
            String password = params[1].toString();
            String name = params[2].toString();
            String birthdate = params[3].toString();
            String gender = params[4].toString();
            String data  = URLEncoder.encode("username", "UTF-8")
                    + "=" + URLEncoder.encode(username, "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8")
                    + "=" + URLEncoder.encode(password, "UTF-8");
            data += "&" + URLEncoder.encode("name", "UTF-8")
                    + "=" + URLEncoder.encode(name, "UTF-8");
            data += "&" + URLEncoder.encode("birthdate", "UTF-8")
                    + "=" + URLEncoder.encode(birthdate, "UTF-8");
            data += "&" + URLEncoder.encode("gender", "UTF-8")
                    + "=" + URLEncoder.encode(gender, "UTF-8");
            URL url = new URL(link);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter
                    (conn.getOutputStream());
            wr.write( data );
            wr.flush();
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            // Read Server Response
            while((line = reader.readLine()) != null)
            {
                sb.append(line);
                break;
            }
            return sb.toString();
        }catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }
    }
    @Override
    protected void onPostExecute(String result){
        if(!result.isEmpty()) {
            c.startActivity(this.HomeActivity);
        }
        else {
            hasil.setVisibility(View.VISIBLE);
            this.hasil.setText("Please choose another username");
        }

    }
}
