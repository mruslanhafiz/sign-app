package com.aufar.help.prototype.teslogin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.aufar.help.prototype.teslogin.PHP.PHPSignIn;


public class SignInActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
    }

    public void DoLogin(View v) {
        EditText usernameText = (EditText)findViewById(R.id.Username);
        EditText passwordText = (EditText)findViewById(R.id.Password);
        TextView hasil = (TextView)findViewById(R.id.hasil);
        hasil.setVisibility(View.INVISIBLE);
        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();
        new PHPSignIn(new Intent(this,CameraActivity

                .class), this, hasil).execute(username,password);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.log_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
