package com.aufar.help.prototype.teslogin;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.aufar.help.prototype.teslogin.PHP.PHPSignUp;

import java.util.Calendar;


public class SignUpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }

    public void DoSignUp(View v) {
        EditText username = (EditText)findViewById(R.id.Username);
        TextView hasil = (TextView)findViewById(R.id.hasil);
        EditText password = (EditText)findViewById(R.id.Password);
        EditText retypepassword = (EditText)findViewById(R.id.ReTypePassword);
        hasil.setText("Password didn't match :(");
        if(retypepassword.getText().toString().equals(password.getText().toString()))
        {
            hasil.setVisibility(View.INVISIBLE);
            EditText birthdate = (EditText)findViewById(R.id.BirthDate);
            EditText fullname = (EditText)findViewById(R.id.FullName);
            Spinner gender = (Spinner)findViewById(R.id.Gender);
            new PHPSignUp(this, new Intent(this, HomeActivity.class),hasil).execute(username.getText().toString(), password.getText().toString(), fullname.getText().toString(), birthdate.getText().toString(), gender.getSelectedItem().toString());
        }
    }

    public void ShowDateDialogue(View v) {
        final Calendar c = Calendar.getInstance();
        final EditText birthdate = (EditText)findViewById(R.id.BirthDate);
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        birthdate.setText(dayOfMonth + "/"
                                + (monthOfYear + 1) + "/" + year);

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
